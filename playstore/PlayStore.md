## Playstore Release Steps

### From Android Studio build Bundle/APK  
a) Go to Build -> Generate signed Bundle/APK -> You can either choose Bundle or APK. We usually use Bundle.  
   b) 
```   
if (you have once built the Bundle from your Android Studio environment) {

    key store passwords etc are saved.  
    so you can go on and click Next until the bundle is generated.  
    
} 
else if (this is the first time you are building bundle from your Android Studio environment 
            but the app exits on Playstore) {
            
    In this case, the credetials should be available in this file in Google Drive
    https://drive.google.com/drive/folders/15YdTzVlMsCXHhPu53dDCmIAWvy5OEhX-?usp=sharing
    
    You can find here the .jks file and in Keystore you can find credentials
    Download the .jks file to your computer
    During generation of Bundle steps, you will be prompted to select a file and the .jks file you downloaded is the 
    one you will select. (Key store path, Choose existing...)
    Also the credentials provided in Keystore file will be the credentials that you will fill.
    
}
else if (this is the first time the app is being deployed on Playstore and nothing exists) {

    In this case to fill the Key store path you will click on Create New...
    Fill the information, chose a password, a path to save .jks file and then generate.
    Do not forget to go to Google Drive and add this new informacion:
    1. Upload the .jks file you generated
    2. Write the password you provided for the app in Keystore document.
    
}
```

### Our current PlayStore Environments
We deploy all our apps usually logged in as contact in our ITWorks Environment.  
The apps that are not in our ITWorks environment are:  
ALBSIG - In their own Playstore environment, which has been given access to us (login as Megi or Ejona)  
SIVIG - In their own Playstore environment, which has been given access to us (login as Megi or Ejona)  
