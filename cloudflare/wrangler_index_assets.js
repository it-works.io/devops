import {getAssetFromKV} from '@cloudflare/kv-asset-handler'

/**
 * The DEBUG flag will do two things that help during development:
 * 1. we will skip caching on the edge, which makes it easier to
 *    debug.
 * 2. we will return an error message on exception in your Response rather
 *    than the default 404.html page.
 */
const DEBUG = false

async function serveAsset(event) {
    const cache = caches.default
    let response = await cache.match(event.request)

    if (!response) {
        console.log(getAssetFromKV(event))
        response = await getAssetFromKV(event)
        const headers = {'cache-control': 'public, max-age=14400'}
        response = new Response(response.body, {...response, headers})
        event.waitUntil(cache.put(event.request, response.clone()))
    }
    return response
}

async function handleRequest(event) {
    if (event.request.method === 'GET') {
        let response = await serveAsset(event)
        if (response.status > 399) {
            response = new Response(response.statusText, {status: response.status})
        }
        return response
    } else {
        return new Response('Method not allowed', {status: 405})
    }
}

addEventListener('fetch', event => {
    try {
        event.respondWith(handleRequest(event))
    } catch (e) {
        if (DEBUG) {
            return event.respondWith(
                new Response(e.message || e.toString(), {
                    status: 500,
                }),
            )
        }
        event.respondWith(new Response('Internal Error', {status: 500}))
    }
})