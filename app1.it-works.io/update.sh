VENV_HOME=venv
echo "Activating virtual environment..."
source $VENV_HOME/bin/activate
echo "Pulling from $1..."
git pull origin $1
echo "Installing requirements..."
pip install -r requirements.txt
#echo "Making migrations..."
#python manage.py migrate
echo "Collecting static content..."
python3 manage.py collectstatic --noinput
