ssh [itworks@app1.it-works.io](mailto:itworks@app1.it-works.io)

# **BACKEND DEPLOY**

## **GITLAB**

First make sure [app1@it-works.io](mailto:app1@it-works.io) is enabled as a deploy key on project repo.

Otherwise the repo will fail to clone to app1.it-works.io

Found under <project-repo> -> Settings -> Repository -> Deploy Keys -> (Privately accessible deploy keys tab - if not enabled -> Enable)

## **ON SERVER**

### **Project repo setup**

```
cd www/production
(production or staging)
$ git clone -b <branch-name> <project-repo> <project-name>
$ cd <project-name>
$ virtualenv venv -p /usr/local/bin/python3.9
$ source venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py collectstatic
$ deactivate
```
_installing requirements might fail for some projects due to package version incompatible with python3.9. Update reqs file locally first using python3.9 environment and then pull again_

### **Uwsgi setup**

```
$ cd ~

$ cd uwsgi/available
$ cp <old_proj...>.ini <new_proj..>.ini
$ vim <new_proj..>.ini
- Change configurations according to current project
```

**Create symbolic link**

```
$ cd ~
$ ln -s /home/itworks/uwsgi/available/<new_proj..>.ini /home/itworks/uwsgi/enabled/
```

Logs located in **uwsgi/logs**

### **Nginx setup**

```
$ cd ~
$ cd nginx/available
$ cp <old_proj..>.conf <new_proj..>.conf
$ vim <new_proj..>.conf
- Change configurations according to current project
```

**Create symbolic link**

```
$ cd ~
$ ln -s /home/itworks/nginx/available/<new_proj..>.conf /home/itworks/nginx/enabled/
```

**Restart servers**

```
$ cd ~
$ ./restart_servers.sh
```

or

```
$ /bin/systemctl restart uwsgi.service
$ /bin/systemctl restart nginx.service
```

## **CLOUDFLARE**

## **CLOUDFLARE**

**Type** will be **CNAME**  
**Content** will be **app1.it-works.io**
<hr/>

# **FRONTEND DEPLOY**

## **ON SERVER**

### **Project repo setup**

```
$ cd www/production
```

(production or staging)

```
$ mkdir <frontend-project-name>
```

### **Nginx setup**

```
$ cd ~
$ cd nginx/available
$ cp <old_proj..>.conf <new_proj..>.conf
$ vim <new_proj..>.conf
- Change configurations according to current project
```

**Create symbolic link**

```
$ cd ~
$ ln -s /home/itworks/nginx/available/<new_proj..>.conf /home/itworks/nginx/enabled/

$ ./restart-servers.sh
```

## **ON WEBSTORM**

1. Go to **Tools -> Deployment -> Configuration**

2. Make sure you are connected to **app1.it-works.io**

3. **Create mapping FROM local path:** ….<project-name>/dist/<project-name>

   **TO:** server path for the frontend project ..home/itworks/production/<project-name>

4. Build project locally with the right environment

   example:
    ```
    ng build --configuration=production
    ```
   (configuration names found under package.json)
5. Go to the project tree **dist** -> <project-name>
   Right click on project name Choose **Deploy to Server** and select **app1.it-works.io**


## **CLOUDFLARE**

**Type** will be **CNAME**  
**Content** will be **app1.it-works.io**

<hr/>

# AUTOMATIC DEPLOY

## Backend

### Add .gitlab-ci.yml file
Follow the example below of **timesheet_backend** deploy script:  
[.gitlab-ci-backend.yml](.gitlab-ci-backend.yml)

Things to consider:
1. PROJECT_NAME
2. tags (are configured on gitlab)  
3. only - refers to branch in which the script will execute on push  
4. url and host  
5. PROJECT_PATH - path on server  
6. **SSH_PRIVATE_KEY should be on environmental variables on gitlab as type FILE**

### Add update.sh file

[update.sh](update.sh)

## Frontend

### Add .gitlab-ci.yml file
Follow the example below of **timesheet-frontend** deploy script:  
[.gitlab-ci-frontend.yml](.gitlab-ci-frontend.yml)

Things to consider:
1. PROJECT_NAME
2. tags (are configured on gitlab)
3. only - refers to branch in which the script will execute on push
4. url and host
5. PROJECT_PATH - path on server
6. **SSH_PRIVATE_KEY should be on environmental variables on gitlab as type FILE**

**IMPORTANT - DEPLOYMENT BRANCH SHOULD BE PROTECTED ON GITLAB**


<hr/>

Configure runners

root@runner1.it-works.io

![](RackMultipart20210920-4-ip6960_html_dac56d905f1d8e8b.png)
