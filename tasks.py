import logging
import os

from invoke import task
from paramiko.client import SSHClient

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

SSH_USER = 'itworks'
DEPLOY_SERVER = 'DEPLOY_SERVER'


def ssh_to_deploy_server():
    deploy_server = os.getenv(DEPLOY_SERVER)
    client = SSHClient()
    client.load_system_host_keys()
    client.connect(hostname=deploy_server, username=SSH_USER)
    return client


def pull_code(ssh_client, deploy_path, project_name, branch):
    pass


def transfer_file_to_ncr1pa(ssh_client, local_filepath, remote_filepath):
    sftp_client = ssh_client.open_sftp()
    sftp_client.put(local_filepath, remote_filepath)
    sftp_client.close()


def initiate_migration(ssh_client, filepath, log_filepath):
    migration_cmd = config.migration_cmd.replace('{migration_file}', filepath).replace('{log_file}', log_filepath)
    stdin, stdout, stderr = ssh_client.exec_command(f'sudo su - wasadmin -c "cd /usr/egov/bindings/uri_ccr/service/bin; {migration_cmd}"')
    logger.info(stdout.readlines())
    logger.error(stderr.readlines())
    ssh_client.close()


def migrate_to_ncr():
    # filename = generate_csv()
    filename = 'citizen_address_migration_TIRANE_24_11_2020.csv'
    ssh_client = connect_to_ncr1pa()
    ncr1pa_filepath = f'{config.migration_remote_dir}/{filename}'
    ncr1pa_log_filepath = f'{config.migration_remote_log_dir}/{filename}'.replace('.csv', '.log')
    transfer_file_to_ncr1pa(ssh_client, filename, ncr1pa_filepath)
    initiate_migration(ssh_client, ncr1pa_filepath, ncr1pa_log_filepath)


migrate_to_ncr()


@task
def ssh_into_server():
    pass


@task
def test(ctx, settings='test_settings'):
    ctx.run(f'coverage run manage.py test --settings={settings} --keepdb')
    ctx.run('coverage report -m')
    ctx.run('coverage xml -o coverage.xml')


@task
def analyze(ctx, sonar_host='http://lan-server:9000', sonar_token='db644b84dc35e80973fdc7f7f5fe73ce2969b1c5'):
    ctx.run(f'docker run --rm --network="host" -e SONAR_HOST_URL={sonar_host} -e SONAR_LOGIN={sonar_token} -v "$(pwd):/usr/src" sonarsource/sonar-scanner-cli')
