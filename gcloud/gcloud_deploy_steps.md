Google Cloud Project Configuration

Frontend: xyz.it-works.io
Backend: xyz-api.it-works.io 

# Steps to do on Cloudflare  
1. Login as contact@it-works.io  
2. Go to it-works.io > DNS  
3. Add **A** record with the subdomain name **xyz** pointing to *192.0.2.1*. Access url: xyz.it-works.io.  
4. Add **CNAME** record with the subdomain name api **xyz-api** pointing to *ghs.googlehosted.com*. Access url: xyz-api.it-works.io  

# Steps to do on Google Cloud Console
1. Go to Google Cloud Console (https://console.cloud.google.com/) logged in with contact@it-works.io.  
    a) Create new GCloud Project  
    b) Enable ComputeEngine API (Search on MarketPlace)  
    c) Enable AppEngine Admin API  
    d) Go to menu AppEngine   
        -> Create application  
        -> Choose python  
        -> Choose europe west6  
    e) Go to menu IAM & Admin  
        -> Go to Service Accounts  
        -> Choose AppEngineDefault service account (should be displayed, if not clear browser cache).  
        -> Add key -> JSON  
        -> Download key  
 
# Steps to do on backend Project  
2. Add package *gunicorn* to requirements.txt  

# Steps to do on gitlab  
3. Realease branch must be protected branch  
4. Go to backend project on Gitlab:  
    a) Go to Settings -> CI/CD    
    b) Go to section *Variables* -> Expand    
    c) Add GCLOUD_PROJECT_ID (project id of the project you created on GCloud) as masked and protected *variable*  
    d) Add GCLOUD_DEPLOY_KEY (the content of the JSON file you downloaded from GCLOUD Console) as protected *file*  
    e) Variable names (GCLOUD_PROJECT_ID and GCLOUD_PROJECT_KEY may vary based on how many different production environment we have)  
    f) If the project deals with file savings (images, docs etc.) add GCLOUD_BUCKET_NAME as protected and masked *variable*. Bucket name is found under GCloud Buckets https://console.cloud.google.com/storage  

# Back to Google Cloud Console
1. Go to AppEngine -> Settings  
2. Custom Domain tab -> Add backend subdomain (xyz-api.it-works.io) -> Choose the appropriate scope -> Write backend app url (configured in Cloudflare). Remove other general scopes. Let it generate certificate (do not wait for loader to finish)  
3. SSL certificates tab -> Choose appropriate certificate  
4. Enable it for you custom domain





