stages:
  - analyze
  - test
  - build
  - deploy

variables:
  VERSION: 0.1.0
  PROJECT_NAME: ${project-name}
  GCLOUD_APP: https://gitlab.com/it-works.io/devops/raw/master/gcloud/app.yaml
  GCLOUD_IGNORE: https://gitlab.com/it-works.io/devops/raw/master/gcloud/.gcloudignore

analyze:
  stage: analyze
  image: sonarsource/sonar-scanner-cli
  tags: [ ${ tag-name } ]
  only:
    - develop
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner

test:
  stage: test
  image: python:3.8
  tags: [ ${ tag-name } ]
  only:
    - develop
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    DB_HOST: $TEST_DB_HOST
    DB_PORT: $TEST_DB_PORT
    DB_USERNAME: $TEST_DB_USERNAME
    DB_PASSWORD: $TEST_DB_PASSWORD
  cache:
    paths:
      - .cache/pip/
  before_script:
    - pip install -r requirements.txt
  script:
    - coverage run manage.py test --settings=$PROJECT_NAME.test_settings --noinput
    - coverage report -m
    - coverage xml -o coverage.xml
  artifacts:
    reports:
      cobertura: coverage.xml

build_static_content_db_demo:
  stage: build
  image: python:3.8
  tags: [ ${ tag-name } ]
  only:
    - demo
  environment:
    name: demo
    url: ${url}
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    GOOGLE_APPLICATION_CREDENTIALS: "service_account.json"
    GS_BUCKET_NAME: "$GCLOUD_DEMO_BUCKET_NAME"
    GCLOUD_DEPLOY_KEY: $GCLOUD_DEMO_DEPLOY_KEY
  cache:
    paths:
      - .cache/pip/
  before_script:
    - echo "$GCLOUD_DEPLOY_KEY" > "$(pwd)/service_account.json"
    - pip install -r requirements.txt
  script:
    - python3 manage.py migrate
    - python3 manage.py collectstatic --noinput

deploy_gcloud_demo:
  stage: deploy
  image: google/cloud-sdk:latest
  tags: [ ${ tag-name } ]
  only:
    - demo
  environment:
    name: demo
    url: ${url}
  variables:
    VERSION: demo
    GCLOUD_PROJECT_ID: $GCLOUD_DEMO_PROJECT_ID
    GCLOUD_DEPLOY_KEY: $GCLOUD_DEMO_DEPLOY_KEY
    GCLOUD_BUCKET_NAME: $GCLOUD_DEMO_BUCKET_NAME
  dependencies:
    - build_static_content_db_demo
  before_script:
    - curl $GCLOUD_APP > app.yaml
    - sed -i 's/${project-name}/'$PROJECT_NAME'/' app.yaml
    - sed -i 's/${db-host}/'$DB_HOST'/' app.yaml
    - sed -i 's/${db-port}/'$DB_PORT'/' app.yaml
    - sed -i 's/${db-username}/'$DB_USERNAME'/' app.yaml
    - sed -i 's/${db-password}/'$DB_PASSWORD'/' app.yaml
    - sed -i 's/${gs-bucket-name}/'$GCLOUD_BUCKET_NAME'/' app.yaml
    - echo "$GCLOUD_DEPLOY_KEY" > "$(pwd)/service_account.json"
    - curl -O $GCLOUD_IGNORE
  script:
    - gcloud auth activate-service-account --key-file="service_account.json"
    - gcloud --quiet --project $GCLOUD_PROJECT_ID app deploy --version=$CI_ENVIRONMENT_SLUG
