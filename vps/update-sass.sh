#Django project update automation script
#!/usr/bin/env bash
VENV_HOME=venv
echo "Activating virtual environment..."
source $VENV_HOME/bin/activate
echo "Collecting static content..."
python3 manage.py collectstatic --noinput
echo "Compiling coffeescript files..."
coffee -c static/
echo "Compiling sass files..."
for sass_folder in "$@"
do
    sass --update static/${sass_folder}/sass:static/${sass_folder}/css
done
